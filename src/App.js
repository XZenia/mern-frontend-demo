import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import CreateTodo from './components/create-todo/create-todo.component';
import EditTodo from './components/edit-todo/edit-todo.component';
import TodosList from './components/todos-list/todos-list.component';
import Header from './components/header/header.component';

function App() {
  return (
    <Router>
      <Header/>
      <Route path="/" exact component={TodosList}/>
      <Route path="/edit/:id" exact component={EditTodo}/>
      <Route path="/create" exact component={CreateTodo} />
    </Router>

  );
}

export default App;
