import React, {Component} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';

import './create-todo.css';

export default class CreateTodo extends Component{
  constructor(props){
     super(props);

     this.onChangeTodoDescription = this.onChangeTodoDescription.bind(this);
     this.onChangeTodoDate = this.onChangeTodoDate.bind(this);
     this.onChangeTodoPriority = this.onChangeTodoPriority.bind(this);
     this.onChangeTodoCompleted = this.onChangeTodoCompleted.bind(this);
     this.onSubmit = this.onSubmit.bind(this);

     this.state = {
       todo_description: '',
       todo_date: new Date(),
       todo_priority:'',
       todo_completed: false
     }
  }

  onChangeTodoDescription(e){
      this.setState({
          todo_description: e.target.value
      });
  }

  onChangeTodoDate = date => {
      this.setState({
        todo_date: date
      });
  };

  onChangeTodoPriority(e){
      this.setState({
          todo_priority: e.target.value
      });
  }

  onChangeTodoCompleted(e){
      this.setState({
          todo_completed: e.target.value
      });
  }

  onSubmit(e){
      e.preventDefault();
      console.log('Form submitted');

      const newTodo = {
          todo_description: this.state.todo_description,
          todo_date: this.state.todo_date,
          todo_priority: this.state.todo_priority,
          todo_completed: this.state.todo_completed
      }

      axios.post('http://127.0.0.1:4000/todos/add',newTodo).then(res => console.log(res.data));

      this.setState({
        todo_description: '',
        todo_date:'',
        todo_priority:'',
        todo_completed: false
      });
  }

  render(){
    return(
        <div class="container">
          <form onSubmit={this.onSubmit}>
            <div class="form-group p-10">
                <label> Description: </label>
                <input type="text" className="form-control" value={this.state.todo_description} onChange={this.onChangeTodoDescription}/>
                <br/>
                <label> Date: &nbsp;&nbsp;</label>
                  <DatePicker
                        selected={this.state.todo_date}
                        onChange={this.onChangeTodoDate}
                        dateFormat="MM-dd-yyyy"
                  />
                <br/>
                <label> Priority: </label>
                <br/>
                <div className="form-check form-check-inline">
                    <input className="form-check-input"
                      type="radio"
                      name="priorityOptions"
                      id="priority_low"
                      value="Low"
                      checked = {this.state.todo_priority ==='Low'}
                      onChange={this.onChangeTodoPriority} />
                    <label className="form-check-label">Low</label>
                </div>
                <div className="form-check form-check-inline">
                    <input className="form-check-input"
                      type="radio"
                      name="priorityOptions"
                      id="priority_medium"
                      value="Medium"
                      checked = {this.state.todo_priority ==='Medium'}
                      onChange={this.onChangeTodoPriority} />
                    <label className="form-check-label">Medium</label>
                </div>
                <div className="form-check form-check-inline">
                    <input className="form-check-input"
                      type="radio"
                      name="priorityOptions"
                      id="priority_high"
                      value="High"
                      checked = {this.state.todo_priority ==='High'}
                      onChange={this.onChangeTodoPriority} />
                    <label className="form-check-label">High </label>
                </div>
            </div>
            <div className="form-group">
              <input type="submit" value="Create Todo" className="btn btn-primary" />
            </div>
          </form>
        </div>
    )
  }
}
