import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './header.css';

export default class Header extends Component{
  render(){
    return(
      <div class="header">
        <nav class="navbar navbar-expand-lg dark bg-dark">
        <img src="/logo192.png" />
        <Link to="/" className="navbar-brand">To-Do List</Link>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <Link className="nav-link" to="/create">Add a Todo</Link>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Settings</a>
          </li>
        </ul>
        </nav>
      </div>
    );
  }
}
