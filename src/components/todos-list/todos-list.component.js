import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import './todos-list.css';

const Todo = props => (
    <tr>
      <td>{props.todo.todo_description}</td>
      <td>{props.todo.todo_date}</td>
      <td>{props.todo.todo_priority}</td>
      <td>{props.todo.todo_completed.toString()}</td>
      <td>
        <Link to={"/edit/"+props.todo._id}>Edit</Link>
      </td>
      <td>
        <a href="#" onClick={() => { props.deleteActivity(props.todo._id) }}>Delete</a>
      </td>
    </tr>
)

export default class TodosList extends Component{

  constructor(props){
    super(props);
    this.deleteActivity = this.deleteActivity.bind(this);
    this.state = {todos: []};
  }

  componentDidMount(){
    axios.get('http://127.0.0.1:4000/todos/')
        .then(response => {
            this.setState({todos: response.data});
        }).catch(function(error){
            console.log(error);
        });
  }

  componentDidUpdate(){
      axios.get('http://127.0.0.1:4000/todos/')
        .then(response => {
            this.setState({todos: response.data});
        }).catch(function(error){
            console.log(error);
        });
  }

  todoList(){
    return this.state.todos.map(currentTodo => {
        return <Todo todo={currentTodo} deleteActivity={this.deleteActivity} key={currentTodo._id} />;
    });
  }

  deleteActivity(id){
    axios.delete('http://127.0.0.1:4000/todos/'+id)
        .catch(function(error){
            console.log(error);
        });
  }


  render(){
    return(
        <div class="content">
          <h2>My TODOs</h2>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Description</th>
                <th>Date</th>
                <th>Priority</th>
                <th>Completed</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              { this.todoList() }
            </tbody>
          </table>
        </div>
    )
  }
}
