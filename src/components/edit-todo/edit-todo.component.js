import React, {Component} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';

export default class EditTodo extends Component{
  constructor(props){
     super(props);

     this.onChangeTodoDescription = this.onChangeTodoDescription.bind(this);
     this.onChangeTodoDate = this.onChangeTodoDate.bind(this);
     this.onChangeTodoPriority = this.onChangeTodoPriority.bind(this);
     this.onChangeTodoCompleted = this.onChangeTodoCompleted.bind(this);
     this.onSubmit = this.onSubmit.bind(this);

     this.state = {
       todo_description:'',
       todo_date: '',
       todo_priority:'',
       todo_completed: false
     }
  }

  componentDidMount(){
    axios.get('http://127.0.0.1:4000/todos/'+this.props.match.params.id)
        .then(response => {
            this.setState({
              todo_description: response.data.todo_description,
              todo_date: response.data.todo_date,
              todo_priority: response.data.todo_priority,
              todo_completed: response.data.todo_completed
            });
        }).catch(function(error){
            console.log(error);
        });
  }

  onChangeTodoDescription(e){
      this.setState({
          todo_description: e.target.value
      });
  }

  onChangeTodoDate = date => {
      this.setState({
        todo_date: date
      });
  };

  onChangeTodoPriority(e){
      this.setState({
          todo_priority: e.target.value
      });
  }

  onChangeTodoCompleted(e){
      this.setState({
          todo_completed: !this.state.todo_completed
      });
  }

  onSubmit(e){
      e.preventDefault();
      console.log('Form submitted');

      const updateTodo = {
          todo_description: this.state.todo_description,
          todo_date: this.state.todo_date,
          todo_priority: this.state.todo_priority,
          todo_completed: this.state.todo_completed
      }

      axios.post('http://127.0.0.1:4000/todos/update/'+this.props.match.params.id, updateTodo).then(res => console.log(res.data));

      this.props.history.push('/');
  }

  render(){
    return(
        <div class="container">
          <form onSubmit={this.onSubmit}>
            <div class="form-group">
                <label> Description: </label>
                <input type="text" className="form-control" value={this.state.todo_description} onChange={this.onChangeTodoDescription}/>
                <br/>
                <label> Date: &nbsp;&nbsp;</label>
                  <DatePicker
                        selected={Date.parse(this.state.todo_date)}
                        onChange={this.onChangeTodoDate}
                        dateFormat="MM-dd-yyyy"
                  />
                <br/>
                <label> Priority: </label>
                <br/>
                <div className="form-check form-check-inline">
                  <input className="form-check-input"
                    type="radio"
                    name="priorityOptions"
                    id="priority_low"
                    value="Low"
                    checked = {this.state.todo_priority ==='Low'}
                    onChange={this.onChangeTodoPriority} />
                  <label className="form-check-label">Low</label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input className="form-check-input"
                      type="radio"
                      name="priorityOptions"
                      id="priority_medium"
                      value="Medium"
                      checked = {this.state.todo_priority ==='Medium'}
                      onChange={this.onChangeTodoPriority} />
                  <label className="form-check-label">Medium</label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input className="form-check-input"
                      type="radio"
                      name="priorityOptions"
                      id="priority_high"
                      value="High"
                      checked = {this.state.todo_priority ==='High'}
                      onChange={this.onChangeTodoPriority} />
                  <label className="form-check-label">High</label>
                </div>
            </div>
            <div className="form-check">
                <input type="checkbox"
                  className="form-check-input"
                  id="completed"
                  name="completed"
                  onChange={this.onChangeTodoCompleted}
                  checked={this.state.todo_completed}
                  value={this.state.todo_completed}/>
                <label className="form-check-label" htmlFor="completed"> Completed? </label>
            </div>
            <br/>
            <div className="form-group">
              <input type="submit" value="Update Todo" className="btn btn-primary" />
            </div>
          </form>
        </div>
    )
  }
}
